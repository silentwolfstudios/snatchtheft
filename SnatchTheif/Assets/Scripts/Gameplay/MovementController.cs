﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    
    public float acceleration = 10f;
    public float initialVelocity = 7f;
    public float maximumVelocity = 20f;    
    public float jumpForce = 9.5f;
    //affect falling only
    public float jumpAirResistance = 80f;
    public float extraGravity = 10f;
    public float timeBetweenAcceleration = 0.3f;
    public float changeLaneTime = 15f;

    public List<GameObject> lanePoints;
    public int currentLaneIndex;
    public int previousLaneIndex;
    public bool canMove = true;
    [SerializeField]
    private Transform targetLanePoint;
    private float nextAccelerationTime = 0;

    //Components
    private Rigidbody myRB;
    private Animator anim;
    private groundDetector GD;
    private HealthController HC;
    private CapsuleCollider capsuleCollider;
    private LaneFollow LF;


    void Awake()
    {
        myRB = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
        GD = GetComponentInChildren<groundDetector>();
        HC = GetComponent<HealthController>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        LF = lanePoints[0].transform.parent.GetComponent<LaneFollow>();

        bool foundCurrentLane = false;

        foreach(GameObject lane in lanePoints){
            if(lane.transform.position.x == transform.position.x){
                foundCurrentLane = true;
                break;
            }
            //check current lane
            currentLaneIndex+=1;
        }
        if(!foundCurrentLane){
            currentLaneIndex -= lanePoints.Count;
        }
    }
    private void OnEnable() {
        // if(transform.rotation != lanePoints[0].transform.parent.rotation && transform.tag == "Player"){
        //     transform.rotation = lanePoints[0].transform.parent.rotation;
        // }
        myRB.velocity = initialVelocity * transform.forward;
    }
    private void OnDisable() {
        myRB.AddForce(new Vector3(0,100,0));
        myRB.velocity = Vector3.zero;
        // force it stop moving by making it heavy
        myRB.mass = 100000;
    }

    void Update(){
    }

    void FixedUpdate()
    {
        Accelerate();
        
        // // rotate according to lane
        // if(transform.rotation != lanePoints[0].transform.parent.rotation && transform.tag == "Player"){
        //     transform.rotation = lanePoints[0].transform.parent.rotation;
        // }

        if(anim && GD)
            anim.SetBool("grounded", GD.grounded);


        if(anim.GetCurrentAnimatorStateInfo(0).IsName("Roll") || (anim.IsInTransition(0) && anim.GetNextAnimatorStateInfo(0).IsName("Roll")) )
        {
            capsuleCollider.center = new Vector3(0,-0.5f,0);
            capsuleCollider.height = 1/2;
        } 
        else
        {
            capsuleCollider.center = new Vector3(0,0,0);
            capsuleCollider.height = 2;            
        }
    }

    private void Accelerate(){
        //acceleration control
        if(canMove){
            if( nextAccelerationTime < Time.time && !(anim.GetCurrentAnimatorStateInfo(0).IsName("Jump") || anim.GetCurrentAnimatorStateInfo(0).IsName("Falling"))){  
                nextAccelerationTime = Time.time + timeBetweenAcceleration; 

                myRB.AddForce(acceleration * transform.forward, ForceMode.Acceleration);    
            }
        }
        else{
            myRB.velocity = Vector3.zero;
        }
        if(Mathf.Abs(myRB.velocity.magnitude) >= maximumVelocity){
            myRB.velocity = maximumVelocity * transform.forward;
        }else if(myRB.velocity.magnitude < 0){
            myRB.velocity = transform.forward;
        }
        anim.SetFloat("speed",myRB.velocity.magnitude);

    
        // gravity multiplier for falling
        myRB.AddForce(extraGravity * myRB.mass * Vector3.down);


        // Lerp move to targetpos
        // if walkingforward
        if(transform.forward == Vector3.forward){
            LF.lockXaxis = true; LF.lockYaxis = true; LF.lockZaxis = false;
            transform.position = Vector3.Lerp(transform.position, new Vector3(targetLanePoint.position.x,transform.position.y,transform.position.z), Time.deltaTime * changeLaneTime );
        } // walkingright 
        else if(transform.forward == Vector3.right){
            LF.lockXaxis = false; LF.lockYaxis = true; LF.lockZaxis = true;
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x,transform.position.y,targetLanePoint.position.z), Time.deltaTime * changeLaneTime );
        }
        
        anim.transform.localRotation = Quaternion.Euler(0,0,0);
        anim.transform.localPosition = new Vector3(0, -0.97f , 0);
    }

    public void ChangeLane(int horinzontalAxisVal){
        previousLaneIndex = currentLaneIndex;
        if(horinzontalAxisVal<0 && anim && currentLaneIndex != 0 ){
            anim.SetTrigger("dodgeleft");  
        }
        if(horinzontalAxisVal>0 && anim && currentLaneIndex != lanePoints.Count-1){
            anim.SetTrigger("dodgeright");  
        }
        print("Hori Axis : " + horinzontalAxisVal + " Lane Index: " + currentLaneIndex);
        currentLaneIndex = Mathf.Clamp( currentLaneIndex + horinzontalAxisVal, 0, lanePoints.Count - 1);
        targetLanePoint = lanePoints[currentLaneIndex].transform;
    }

    public void JumpSlide(float verticalAxisVal){
        if(GD.grounded){
            if(verticalAxisVal>0 && anim ){
                anim.SetTrigger("jump");
                myRB.AddForce(jumpForce * verticalAxisVal * Vector3.up , ForceMode.Impulse   );
                
                // jump air resistance 
                if(myRB.velocity.z * transform.forward.z >=10f){
                    myRB.AddForce(jumpAirResistance * myRB.velocity.z/maximumVelocity * -transform.forward);
                }
            }
        } 
        if(verticalAxisVal<0 && anim){
            anim.SetTrigger("roll");  
            if(!GD.grounded){
                myRB.AddForce(jumpForce * verticalAxisVal * Vector3.up , ForceMode.Impulse   );
                
                // jump air resistance
                if(myRB.velocity.z * transform.forward.z >=10f){
                    myRB.AddForce(jumpAirResistance * myRB.velocity.z/maximumVelocity * -transform.forward);
                }
            }
        }

    }

    private void OnCollisionEnter(Collision other) {
        if(other.collider.tag == "Obstacle" || other.collider.tag == "Enemy"){
                HC.ApplyDamage(1);

                if(currentLaneIndex >= lanePoints.Count-1){
                    currentLaneIndex--;
                } 
                else if(currentLaneIndex<=0){
                    currentLaneIndex++;
                }
                // when not on either edge lane
                else if(previousLaneIndex>currentLaneIndex){
                    currentLaneIndex--;
                } else if(previousLaneIndex<currentLaneIndex){
                    currentLaneIndex++;

                } else
                {
                    //50% up one index or down when not at edge lane
                    if(Random.Range(0,10)>5 && !(currentLaneIndex<=0 && currentLaneIndex>=lanePoints.Count-1)){
                        currentLaneIndex++;
                    } 
                    else if(!(currentLaneIndex<=0 && currentLaneIndex>=lanePoints.Count-1)) {
                        currentLaneIndex--;
                    }
                }
                
                //Anim only
                if(previousLaneIndex > currentLaneIndex){
                    anim.SetTrigger("hitR");
                } else if(previousLaneIndex < currentLaneIndex){
                    anim.SetTrigger("hitL");
                } else if(previousLaneIndex == currentLaneIndex){
                    anim.SetTrigger("hitF");
                }
        }
    }
    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Projectile"){
            HC.ApplyDamage(1);
        }    
    }


}
