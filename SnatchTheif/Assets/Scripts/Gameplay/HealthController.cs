﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthController : MonoBehaviour
{
    public float maxHP = 5f; 
    // surface represent size of water flow  and HP 
    public float currentHP = 5f; 
    public Slider HPSlider;
    public TextMeshPro HPText;
    //protect player 
	private bool isShielded;
    private Animator anim;
    private SkinnedMeshRenderer skinnedMeshRenderer;
    private InputController IC;
    private MovementController MC;
    private GameManager GM;
    void Start()
    {
        IC = GetComponent<InputController>();
        MC = GetComponent<MovementController>();
        GM = GameManager.instance;
        anim = GetComponentInChildren<Animator>();
        skinnedMeshRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
        currentHP = maxHP;
        HPText.text = currentHP.ToString();
    
    }

	public bool Shielded{
		get {return isShielded;}
		set {isShielded= value;}
	}
    public void ApplyDamage(float DamageValue){
        if(currentHP>0 && !Shielded){
            currentHP-= DamageValue;
            if (gameObject.activeInHierarchy) {
                StartCoroutine (TurnOffOnSign());
            }
        } 
        if(currentHP<=0){
            anim.SetTrigger("death");
            IC.enabled = false;
            MC.enabled = false;
            if(tag == "Player")
                GM.StartCoroutine (GM.GameOver());
            currentHP = 0;
        }
        HPSlider.value = currentHP/maxHP;
        HPText.text = currentHP.ToString();
    }
    public void ApplyDamageWithoutShield(float DamageValue){
        if(currentHP>0){
            currentHP-= DamageValue;
            // if (gameObject.activeInHierarchy) {
            //     StartCoroutine (TurnOffOnSign());
            // }
        } 
        if(currentHP<=0){
            anim.SetTrigger("death");
            IC.enabled = false;
            MC.enabled = false;
            if(tag == "Player")
                GM.StartCoroutine (GM.GameOver());            
            currentHP = 0;
            GetComponent<CapsuleCollider>().radius = 0.1f;
        }
        HPSlider.value = currentHP/maxHP;
        HPText.text = currentHP.ToString();
    }
    public void Heal(float HealValue){
        if(currentHP>0){
            currentHP+= HealValue;
        }
        if(currentHP>maxHP){
            currentHP = maxHP;
        }
        HPSlider.value = currentHP/maxHP;
        HPText.text = currentHP.ToString();
    }

    private void OnCollisionEnter(Collision other) {
        if(other.collider.tag == "Obstacle"){
            ApplyDamage(1);
        }

    }
	IEnumerator TurnOffOnSign() {
		isShielded = true;
		for(int i =0; i<5; i++){
			yield return new WaitForSeconds (0.1f);
            skinnedMeshRenderer.enabled = false;
			yield return new WaitForSeconds (0.1f);
            skinnedMeshRenderer.enabled = true;
		}
		isShielded = false;
	}


}
