﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleCardController : MonoBehaviour
{
    public List<Button> BattleCards;
    
    private GameObject player;
    public List<bool> holdingBC;
    public static BattleCardController instance;
    
    
    private Animator anim;
    void Start(){
        player = GameObject.FindGameObjectWithTag("Player");
        anim = player.GetComponentInChildren<Animator>();
        instance = this;
        
        foreach (var battleCard in BattleCards)
        {
            holdingBC.Add(!battleCard.IsActive());
        }
    }

    void Update() {
        if(holdingBC[0]){
            player.GetComponentInChildren<fireBullet>().useWeapon();
        }
        if(holdingBC[1]){
            player.GetComponentInChildren<meleeScript>().useWeapon();
        }
    }

    public void OnBattleCard_1Up(){
        holdingBC[0] = false;
    }
    public void OnBattleCard_1Down(){
        holdingBC[0] = true;
    }
    public void OnBattleCard_2Up(){
        holdingBC[1] = false;
    }
    public void OnBattleCard_2Down(){
        holdingBC[1] = true;        
    }
    public void OnBattleCard_3Up(){
        holdingBC[2] = false;
    }
    public void OnBattleCard_3Down(){
        holdingBC[2] = true;                
    }
    public void OnBattleCard_4Up(){
        holdingBC[3] = true;
    }
    public void OnBattleCard_4Down(){
        holdingBC[3] = false;
    }    
}
