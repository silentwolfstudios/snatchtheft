﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyDamage : MonoBehaviour
{
    public float damage;
    public float damageRate;
    public float pushBackForce;

    float nextDamage; // time when damage is possible

    bool playerInRange = false;

    GameObject thePlayer;
    HealthController HC;

    // Start is called before the first frame update
    void Start()
    {
        nextDamage = Time.time;
        thePlayer = GameObject.FindGameObjectWithTag("Player");
        HC = thePlayer.GetComponent<HealthController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(playerInRange) Attack(); 
    }

    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Player"){ // issue with nested collider on older unity versions, so we do it this way
            playerInRange = true;
        }    
    }

    private void OnTriggerExit(Collider other) {
         if(other.tag == "Player"){ // issue with nested collider on older unity versions, so we do it this way
            playerInRange = false;
        }           
    }

    void Attack(){
        if(HC != null){
            if(nextDamage<= Time.time){
                HC.ApplyDamage(damage);
                nextDamage = Time.time + damageRate;
                
                pushback(thePlayer.transform);
            }
        }
    }

    void pushback(Transform pushedObject){
        Vector3 pushDirection = new Vector3(0, (pushedObject.position.y - transform.position.y),0 ).normalized; // normalized returns unit vector
        pushDirection*=pushBackForce;

        Rigidbody pushedRB = pushedObject.GetComponent<Rigidbody>();
        pushedRB.velocity = Vector3.zero;
        pushedRB.AddForce(pushDirection, ForceMode.Impulse); // impulse is the explosive type of force

    }


}