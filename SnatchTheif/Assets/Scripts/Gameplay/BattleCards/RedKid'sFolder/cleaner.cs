﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cleaner : MonoBehaviour
{
    HealthController HC;

    // Update is called once per frame
    void Start()
    {
        HC = GameObject.FindGameObjectWithTag("Player").GetComponent<HealthController>();
    }

    private void OnTriggerEnter(Collider other) {
        if(other.tag=="Player"){
            HC.ApplyDamage(100000f); // instant kill
        } else Destroy(other.gameObject );
    }
}
