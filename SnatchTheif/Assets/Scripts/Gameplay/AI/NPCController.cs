﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController : MonoBehaviour
{   
    private Vector3 initialLocalPos;
    private HealthController HC;
    void Start(){
        initialLocalPos = transform.localPosition;
        HC = GetComponent<HealthController>();
    
    
    }
    void OnEnable(){
        transform.localPosition = initialLocalPos;
        HC.currentHP = HC.maxHP;
    }
}
