﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDeactiveTile : MonoBehaviour
{


    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Player"){
            transform.parent.gameObject.SetActive(false);
            transform.parent.transform.position = ObjectPooler.instance.hiddenPos;

  
        }
    }
}
