﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaneFollow : MonoBehaviour
{
    public GameObject target;
    public bool lockXaxis;
    public bool lockYaxis;
    public bool lockZaxis;



    // Update is called once per frame
    void Update()
    {
        float followPosX = target.transform.position.x;
        float followPosY = target.transform.position.y;
        float followPosZ = target.transform.position.z;
        if(lockXaxis){
            followPosX = transform.position.x;
        }
        if(lockYaxis){
            followPosY = transform.position.y;

        }
        if(lockZaxis){
            followPosZ = transform.position.z;

        }
        transform.position = new Vector3(followPosX,followPosY,followPosZ);


    }
}
