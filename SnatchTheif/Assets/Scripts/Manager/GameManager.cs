﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    public int Score;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI hiscoreText;

    public GameObject gameOverUI;
    public GameObject deathSprite;

    public static GameManager instance;

    private GameObject player;
    private GameObject spawnedDeathSprite;
    
    void Awake() {
        instance = this;    
        player = GameObject.FindGameObjectWithTag("Player");
        Score = 0;
    }


    public void IncreaseScore(int value){
        if(scoreText != null){
            Score += value;
            scoreText.text = Score.ToString();
        }
    }
    void StoreHighscore(int newHighscore)
    {
        int oldHighscore = PlayerPrefs.GetInt("highscore", 0);    
        if(newHighscore > oldHighscore){
            PlayerPrefs.SetInt("highscore", newHighscore);
            hiscoreText.text = newHighscore.ToString();
        } else{
            hiscoreText.text = oldHighscore.ToString();
        }
            PlayerPrefs.Save();

    }

    public IEnumerator GameOver(){
        StoreHighscore(Score);
        yield return new WaitForSeconds(1.5f);
        if(gameOverUI !=null){
            gameOverUI.SetActive(true);
            print("GameOver");
        }
        // //if death sprite has not exist yet
        // if(spawnedDeathSprite == null){
        //     spawnedDeathSprite = Instantiate(deathSprite, player.transform.position, Quaternion.identity);
        //     spawnedDeathSprite.GetComponent<Animator>().SetTrigger("GameOver");
        // }
    }


}
