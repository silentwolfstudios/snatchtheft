﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectTransition : MonoBehaviour
{
    public GameObject inPoint;

    public GameObject outLeftPoint;
    public GameObject outRightPoint;
    public List<GameObject> PatrolPoints;
    public int index;

    public float transitionSpeed = 1f;
    
    public Vector3 targetPos;
    
    private InputController IC;



    void Start()
    {
        if(PatrolPoints.Count > 0){
            targetPos = PatrolPoints[index].transform.position;
        } else{
            targetPos = transform.position;
        }
        IC = GetComponent<InputController>();
    }


    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, targetPos,Time.deltaTime * transitionSpeed);     
        
        // Swipe Right
        if(Input.GetMouseButtonDown(2) || IC.swipeHorizontalAxis > 0.5){
            index += 1;
            if(index >= PatrolPoints.Count){
                index = PatrolPoints.Count-1;
            }     
            AnimateToPoint(index);
        } 
        //Swipe Left
        if(Input.GetMouseButtonDown(1) || IC.swipeHorizontalAxis < -0.5){
            index -= 1;
            if(index <= 0){
                index = 0;
            }     
            AnimateToPoint(index);
        } 
    }

    void AnimateIn(){
        if(inPoint)
            targetPos = inPoint.transform.position;
    }
    void AnimateOutLeft(){
        if(outLeftPoint)
            targetPos = outLeftPoint.transform.position;
    }
    void AnimateOutRight(){
        if(outRightPoint)
            targetPos = outRightPoint.transform.position;
    }

    void AnimateToPoint(int indexVal){
        if(PatrolPoints.Count > 0){
            targetPos = PatrolPoints[indexVal].transform.position;
        }
    }
}

