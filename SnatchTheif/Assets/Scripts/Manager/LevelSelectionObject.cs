﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectionObject : MonoBehaviour
{
    public int levelIndex;
    private void OnMouseUp() {
        LevelManager.instance.LoadLevelByIndex(levelIndex);
    }
}
